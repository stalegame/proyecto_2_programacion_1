def mostrar_pais_especifico(diccionario):

    """ Como ya se creo el diccionario con las llaves respectivas que seran
        los paises se puede hacer la comparacion de este con los archivos
        y lograr obtener la informacion correspondiente"""

    pais_seleccionado = input("Seleccione el pais a mostrar: ").capitalize()

    # se guarda el dia anterior para comprobar si paso o no de mes
    anterior = diccionario[pais_seleccionado]['registro'][0]

    for nombre in diccionario[pais_seleccionado]['registro']:

        # Compara el dia actual con el anterior
        if nombre[2].split('-')[1] != anterior[2].split('-')[1]:
            print(f"{nombre[0]}:")
            print(f"año: {nombre[2].split('-')[0]}")
            print(f"mes: {nombre[2].split('-')[1]}")
            print(f"Número absoluto de inmunizaciones: {nombre[3]}")
            print(f"Número total de personas vacunadas: {nombre[4]}")
            print("Número total de personas con el esquema"
                  f" completo: {nombre[5]}")

            anterior = nombre

            # comprueba si existen datos para mostrar del segundo csv
            if "vacunas" in diccionario[pais_seleccionado]:

                total = 0
                for vacunas in diccionario[pais_seleccionado]['vacunas']:

                    # Compara el dia actual con el anterior
                    if vacunas[1].split('-') == anterior[2].split('-'):
                        total += float(vacunas[3])
                        print(f"Vacunas utilizada: {vacunas[2]}({vacunas[3]})")

                print(f"Merma de vacunas: {float(total) - float(nombre[3])}")
            print("")


def mostrar_pais_fecha(diccionario):

    """ basicamente se reutiliza gran parte del primer punto, agregando
       netamente una fecha en especifico para ver de algun pais"""

    pais_seleccionado = input("Seleccione el pais a mostrar: ").capitalize()
    fecha_seleccionada = input("Seleccione una fecha(YYYY-MM-DD): ").split("-")
    print("")

    for nombre in diccionario[pais_seleccionado]['registro']:

        # Compara el dia actual con el anterior
        if nombre[2].split("-") == fecha_seleccionada:
            print(f"{nombre[0]}:")
            print(f"año: {nombre[2].split('-')[0]}")
            print(f"mes: {nombre[2].split('-')[1]}")
            print(f"dia: {nombre[2].split('-')[2]}")
            print(f"Número absoluto de inmunizaciones: {nombre[3]}")
            print(f"Número total de personas vacunadas: {nombre[4]}")
            print("Número total de personas con el esquema"
                  f" completo: {nombre[5]}")

            # comprueba si existen datos para mostrar del segundo csv
            if "vacunas" in diccionario[pais_seleccionado]:
                total = 0
                for vacunas in diccionario[pais_seleccionado]['vacunas']:

                    # Compara el dia actual con el anterior
                    if vacunas[1].split("-") == fecha_seleccionada:
                        total += float(vacunas[3])
                        print(f"Vacunas utilizada: {vacunas[2]}({vacunas[3]})")

                print(f"Merma de vacunas: {float(total) - float(nombre[3])}")
            print("")


def comparar_pais(diccionario):

    """ Y al igual que la funcion anterior se reutiliza gran parte, para
        mostrar al usario una comparacion de datos de dos paises en una misma
        fecha y mostrando los datos"""

    pais_seleccionado_1 = input("Seleccione el pais a mostrar: ").capitalize()
    pais_seleccionado_2 = input("Seleccione el pais a mostrar: ").capitalize()
    fecha_seleccionada = input("Seleccione una fecha(YYYY-MM-DD): ").split("-")
    print("")

    for nombre in diccionario[pais_seleccionado_1]['registro']:

        # Compara el dia actual con el seleccionado
        if nombre[2].split("-") == fecha_seleccionada:
            print(f"{nombre[0]}:")
            print(f"año: {nombre[2].split('-')[0]}")
            print(f"mes: {nombre[2].split('-')[1]}")
            print(f"dia: {nombre[2].split('-')[2]}")
            print(f"Número absoluto de inmunizaciones: {nombre[3]}")
            print(f"Número total de personas vacunadas: {nombre[4]}")
            print("Número total de personas con el esquema"
                  f" completo: {nombre[5]}")

            # comprueba si existen datos para mostrar del segundo csv
            if "vacunas" in diccionario[pais_seleccionado_1]:
                total = 0
                for vacunas in diccionario[pais_seleccionado_1]['vacunas']:

                    # Compara el dia actual con el seleccionado
                    if vacunas[1].split("-") == fecha_seleccionada:
                        total += float(vacunas[3])
                        print(f"Vacunas utilizada: {vacunas[2]}({vacunas[3]})")

                print(f"Merma de vacunas: {float(total) - float(nombre[3])}")
            print("")

    for nombre in diccionario[pais_seleccionado_2]['registro']:

        # Compara el dia actual con el seleccionado
        if nombre[2].split("-") == fecha_seleccionada:
            print(f"{nombre[0]}:")
            print(f"año: {nombre[2].split('-')[0]}")
            print(f"mes: {nombre[2].split('-')[1]}")
            print(f"dia: {nombre[2].split('-')[2]}")
            print(f"Número absoluto de inmunizaciones: {nombre[3]}")
            print(f"Número total de personas vacunadas: {nombre[4]}")
            print("Número total de personas con el esquema"
                  f" completo: {nombre[5]}")

            # comprueba si existen datos para mostrar del segundo csv
            if "vacunas" in diccionario[pais_seleccionado_2]:
                total = 0
                for vacunas in diccionario[pais_seleccionado_2]['vacunas']:

                    # Compara el dia actual con el seleccionado
                    if vacunas[1].split("-") == fecha_seleccionada:
                        total += float(vacunas[3])
                        print(f"Vacunas utilizada: {vacunas[2]}({vacunas[3]})")

                print(f"Merma de vacunas: {float(total) - float(nombre[3])}")
            print("")


def menu(diccionario):

    """ Basicamente se crea el menu, para ser más practivo y visible para el
        usario, donde dependiendo la eleccion podra ir navegando en las
        opciones que dispone el programa"""

    contador = 1
    while contador != 0:
        print(" < 0 > finalizar programa")
        print(" < 1 > Busqueda pais")
        print(" < 2 > Busqueda pais con fecha")
        print(" < 3 > comparacion de paises (nombre y fecha)\n")
        contador = int(input("Ingrese el que desee utilizar: "))
        print("")

        if contador == 1:
            mostrar_pais_especifico(diccionario)

        elif contador == 2:
            mostrar_pais_fecha(diccionario)

        elif contador == 3:
            comparar_pais(diccionario)

        elif contador == 0:
            print("gracias por utilizar el programa")


def crear_json(archivo=None, archivo_2=None):

        """Sabiendo la estructura de los "cvs" decidi pasar cada fila a una
           lista y separando cada columna que tuviera una comilla, para luego
           usarlos como indices de un diccionario"""

        datos = [linea.split(",")for linea in archivo]
        datos_2 = [linea.split(",")for linea in archivo_2]

        diccionario = {}

        """El primer for itera en las listas creadas anteriormente
           introducciondo la posiccion 0, que serian los paises como keys
           al igual de crear el formato de diccionario y agregando dichos
           paises sin repetirse"""

        for linea in datos:
            if linea[0] not in diccionario:

                diccionario[linea[0]] = {}
                diccionario[linea[0]]["registro"] = []
            diccionario[linea[0]]["registro"].append(linea)
        datos_2.pop(0)

        """Y en el siguiente for ingresaria el segundo archivo con la clave
           vacunas, como ya se crearon las keys, estas se guardarian junto a
           las keys respectivas, en forma de lista"""

        for linea in datos_2:
            if "vacunas" not in diccionario[linea[0]]:

                diccionario[linea[0]]["vacunas"] = []
            diccionario[linea[0]]["vacunas"].append(linea)

        return diccionario


def cargar_archivo(archivo=None):

    """La funcion se encarga de abrir los dos archivos, en caso de no
       encontrarlos se le informara al usario"""

    try:
        with open(archivo) as archivo:
            datos = [x.strip() for x in archivo.readlines()]
            return datos
    except FileNotFoundError:
        print("Error, el archivo no fue encontrado")


def main():
    archivo_1 = "country_vaccinations.csv"
    archivo_2 = "country_vaccinations_by_manufacturer.csv"
    vacunacion_pais = cargar_archivo(archivo_1)
    vacunacion_pais_fabricante = cargar_archivo(archivo_2)
    diccionario = crear_json(vacunacion_pais, vacunacion_pais_fabricante)
    menu(diccionario)


if __name__ == "__main__":
    main()
